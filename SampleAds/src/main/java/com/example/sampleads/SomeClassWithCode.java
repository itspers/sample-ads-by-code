package com.example.sampleads;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

/**
 * Created by pers on 9/20/13.
 */


public class SomeClassWithCode {
    public static void methodThatAddAds(Activity act){

        //OK. Now this AIR generator made some surface for AIR app and give you access to activity
        // i put just image with screenshot of app in main layout

        //1. this will be new main layout
        //match parent make it 100% height and width
        RelativeLayout rl = new RelativeLayout(act);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT,android.widget.RelativeLayout.LayoutParams.MATCH_PARENT);
        rl.setLayoutParams(params);


        //get first child of whole app layout (AIR surface should be here)
        ViewGroup fl = (ViewGroup)act.findViewById(android.R.id.content);
        View contents = fl.getChildAt(0);
        //detach it from current parent
        fl.removeView(contents);
        //attach to new parent
        rl.addView(contents);


        //this will be holder for adview, just to add background color (cause it can be smaller then device screen width)
        //will be like just white rectangle
        //WRAP_CONTENT on height make it change height depending on contents inside
        LinearLayout adViewHolder = new LinearLayout(act);
        RelativeLayout.LayoutParams adViewHolderparams = new RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT,android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
        //this will be margin from bottom
        adViewHolderparams.setMargins(0,0,0,75);
        //this align to bottom
        adViewHolderparams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adViewHolder.setLayoutParams(adViewHolderparams);
        //white background
        adViewHolder.setBackgroundColor(act.getResources().getColor(android.R.color.white));


        // Create the adView
        //here i use normal admob android sdk, you need to use own class insetad of AdView
        AdView adView = new AdView(act, AdSize.BANNER, "a14f36e7c3c799c");
        RelativeLayout.LayoutParams paramsAd = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
        adView.setLayoutParams(paramsAd);

        //add ads view to holder layout
        adViewHolder.addView(adView);

        //now add holder to new main
        rl.addView(adViewHolder);

        //so now we have main RelativeLayout with old content + adview

        //replace whole activity layout with new one
        act.setContentView(rl);

        //ask admob for ads (maybe not using in your sdk)
        adView.loadAd(new AdRequest());
    }
}
